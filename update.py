import psycopg2
from config import config


def update_vendor(vendor_id, vendor_name):
    query = """UPDATE vendors SET vendor_name = %s WHERE vendor_id=%s"""
    conn = None
    update_rows = 0
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (vendor_name, vendor_id))
        update_rows = cur.rowcount
        conn.commit()
        
        cur.close()
    except(Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        conn.close()


if __name__ == '__main__':
    # Update vendor id 1
    update_vendor(1, "3M Corp")