import psycopg2
from config import config

def connect():
    conn = None

    try:
        params = config()
        conn = psycopg2.connect(**params)

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

    finally:
        if conn is not None:
            conn.close()

if __name__ == '__main__':
    connect()